/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TetravexSolver;
import java.util.Iterator;
/**
 *
 * @author bryam
 * @param <M>
 */
public interface State<M> {
    /**
	 * Returns true if this state is a solution to the problem, else false.
     * @return 
	 */
	public boolean isSolution();

	/**
	 * Returns a list of all possible moves for this state.
     * @return 
	 */
	public Iterator<M> getMoves();

	/**
	 * Gets an exact copy of this State.
     * @return 
	 */
	public State<M> getCopy();

	/**
	 * Applies the given Move to this State.
	 * 
	 * @param move
	 *        The move to apply.
	 */
	public void addMove( M move );

	/**
	 * Sets the parent state.
	 * 
	 * @param parent
	 *        The parent of this state.
	 */
	public void setParent( State<M> parent );

	/**
	 * Returns the parent state.
	 */
	public State<M> getParent();

	/**
	 * Sets the depth of this state.
	 * 
	 * @param depth
	 *        The depth of this state.
	 */
	public void setDepth( int depth );

	/**
	 * Returns the depth of this state.
	 */
	public int getDepth();

	/**
	 * Returns the hash of this State. The hash object returned must have the
	 * hashCode() and equals() methods implemented to accurately compare against
	 * another hash object. If state A and B are the same state, then their hash
	 * objects will having matching hashCode() values and A.equals(B) will return
	 * true (same applies for B.equals(A)).
	 */
	public Object getHash();
	
	/**
	 * Tries to estimate the minimum number of remaining moves until the state
	 * is solved. If this can't be calculated then -1 should be returned. This
	 * is especially useful when you set a maximum search depth in a Solver
	 * and you want to weed out states that don't have a chance being solvable
	 * in time.
	 * 
	 * @return
	 */
	public int getRemainingMoves();
}
