/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TetravexSolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;



/**
 *
 * @author bryam
 */

public class TetravexBoard extends AbstractState<TetravexMove>{
    private Pieza[] pieces;

	private boolean[] placed;
	private int width;
	private int height;
	private int placedCount;
	private int[][] board;
        private static long asig;
        private static long comp;

	private TetravexBoard()
	{
	}

	public TetravexBoard( int width, int height, Pieza[] pieces, int[][] board )
	{
		this.width = width;
		this.height = height;
		this.pieces = pieces;
		this.board = board;
		this.placedCount = 0;
		this.placed = new boolean[width * height];
		
		for (int i = 0; i < pieces.length; i++)
		{
			pieces[i].index = i;
		}
		
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				if (board[y][x] >= 0)
				{
					placedCount++;
					placed[board[y][x]] = true;
				}
			}
		}
	}

	public boolean exists( int x, int y )
	{
		return !(x < 0 || y < 0 || x >= width || y >= height);
	}

	public boolean alone( int x, int y )
	{
		return (
				(!exists( x - 1, y ) || board[y][x - 1] == Pieza.EMPTY) &&
				(!exists( x + 1, y ) || board[y][x + 1] == Pieza.EMPTY) &&
				(!exists( x, y - 1 ) || board[y - 1][x] == Pieza.EMPTY) && 
				(!exists( x, y + 1 ) || board[y + 1][x] == Pieza.EMPTY)
		);
	}

	public boolean isPiece( int x, int y )
	{
		return (exists( x, y ) && board[y][x] != Pieza.EMPTY);
	}

	private Pieza getIdeal( int x, int y )
	{
		Pieza ideal = new Pieza( Pieza.EMPTY, Pieza.EMPTY,
                        Pieza.EMPTY, Pieza.EMPTY );
		
		if (isPiece( x - 1, y ))
		{
			ideal.s[Pieza.LEFT] = pieces[board[y][x - 1]].s[Pieza.RIGHT];
		}
		if (isPiece( x + 1, y ))
		{
			ideal.s[Pieza.RIGHT] = pieces[board[y][x + 1]].s[Pieza.LEFT];
		}
		if (isPiece( x, y - 1 ))
		{
			ideal.s[Pieza.TOP] = pieces[board[y - 1][x]].s[Pieza.BOTTOM];
		}
		if (isPiece( x, y + 1 ))
		{
			ideal.s[Pieza.BOTTOM] = pieces[board[y + 1][x]].s[Pieza.TOP];
		}
		
		return ideal;
	}

	@Override
	public Iterator<TetravexMove> getMoves()
	{
		List<TetravexMove> moves = new ArrayList<TetravexMove>();

		// Handle the case of an empty board
		if (placedCount == 0)
		{
			for (int i = 0; i < placed.length; i++)
			{
				moves.add( new TetravexMove( i, 0, 0 ) );
			}
			return moves.iterator();
		}

		// In worst case there will be size^2 number of matches, which means
		// the board is empty.
		int minMatches = width * height + 1;

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				// If this place is empty but NOT alone (has no neighbors)
				if (board[y][x] == Pieza.EMPTY && !alone( x, y ))
				{
					// Determine the ideal piece. The ideal piece can have
					// negative sides noting that anything can match this side
					Pieza ideal = getIdeal( x, y );
					// Find all remaining pieces that can fit here
					List<Pieza> matches = new ArrayList<Pieza>();
					
					for (int i = 0; i < placed.length; i++)
					{
						if (!placed[i] && pieces[i].match( ideal ))
						{
							matches.add( pieces[i] );
						}
					}
					
					// If the number of matches for this is the next best
					// position then update the number of moves.
					if (matches.size() < minMatches)
					{
						minMatches = matches.size();
						moves.clear();
						
						for (Pieza p : matches)
						{
                                                    try{
							moves.add( new TetravexMove( p.index, x, y ) );
                                                    }catch(Exception e){
                                                        System.out.println("AQUIII");
                                                    }
						}
					}
				}
			}
		}
		
		return moves.iterator();
	}

	@Override
	public void addMove( TetravexMove move )
	{
		placed[move.pieceIndex] = true;
		board[move.y][move.x] = move.pieceIndex;
		placedCount++;
	}

	@Override
	public State<TetravexMove> getCopy()
	{
		TetravexBoard copy = new TetravexBoard();
		copy.width = width;
		copy.height = height;
		copy.pieces = pieces;
		copy.placedCount = placedCount;
		copy.placed = Arrays.copyOf( placed, placed.length );
		copy.board = new int[height][];
		
		for (int i = 0; i < height; i++)
		{
                    try{
			copy.board[i] = Arrays.copyOf( board[i], board[i].length );
                    }catch(Exception e){
                        System.out.println("AQUÍ");
                    }
		}
		
		return copy;
	}

	@Override
	public boolean isSolution()
	{
		return (placedCount == width * height);
	}

	@Override
	public Object getHash()
	{
		return null;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				sb.append( "+-----" );
			}
			sb.append( "+\n" );

			for (int x = 0; x < width; x++)
			{
				sb.append( "|" );
				if (board[y][x] >= 0)
				{
					Pieza p = pieces[board[y][x]];
					sb.append( "\\ " ).append( p.s[Pieza.TOP] ).append( " /" );
				}
				else
				{
					sb.append( "     " );
				}
			}
			sb.append( "|\n" );

			for (int x = 0; x < width; x++)
			{
				sb.append( "|" );
				if (board[y][x] >= 0)
				{
					Pieza p = pieces[board[y][x]];
					sb.append( p.s[Pieza.LEFT] ).append( " X " ).append( p.s[Pieza.RIGHT] );
				}
				else
				{
					sb.append( "     " );
				}
			}
			sb.append( "|\n" );

			for (int x = 0; x < width; x++)
			{
				sb.append( "|" );
				if (board[y][x] >= 0)
				{
					Pieza p = pieces[board[y][x]];
					sb.append( "/ " ).append( p.s[Pieza.BOTTOM] ).append( " \\" );
				}
				else
				{
					sb.append( "     " );
				}
			}
			sb.append( "|\n" );
		}

		for (int x = 0; x < width; x++)
		{
			sb.append( "+-----" );
		}
		sb.append( "+\n" );

		return sb.toString();
	}
    
}
