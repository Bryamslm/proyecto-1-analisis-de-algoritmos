/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TetravexSolver;

/**
 *
 * @author bryam
 */
public class TetravexMove {
    public int pieceIndex;
	public int x, y;

	public TetravexMove( int pieceIndex, int x, int y )
	{
		this.pieceIndex = pieceIndex;
		this.x = x;
		this.y = y;
	}
}
