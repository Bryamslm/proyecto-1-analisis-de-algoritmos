/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TetravexSolver;

import java.util.ArrayDeque;

/**
 *
 * @author bryam
 */
public class Deque<E> extends ArrayDeque<E> {
    // First-In-First-Out?
	private boolean fifo = true;

	/**
	 * Instantiates a new Deque.
	 * 
	 * @param fifo
	 *        True if this Deque is first-in-first-out, or false if this Deque is
	 *        first-in-last-out.
	 */
	public Deque( boolean fifo )
	{
		this.fifo = fifo;
	}

	@Override
	public E poll()
	{
		return (fifo ? pollFirst() : pollLast());
	}

	@Override
	public boolean add( E element )
	{
		return (fifo ? offerLast( element ) : offerFirst( element ));
	}

}
