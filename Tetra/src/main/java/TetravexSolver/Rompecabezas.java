/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TetravexSolver;

import static java.lang.Math.random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 *
 * @author bryam
 */
public class Rompecabezas{
    private Pieza rompecabezas[][];
    private List<Pieza> desordenado;
    private List<Pieza> ordenado;
    private int numComlunas;
    private long asignaciones=0;
    private long comparaciones=0;
    private static int numPieza=0;
    private int[][] board;
    private int[][] boardBruto;
    private Pieza[] piezas;
    private int finas, columnas;
    

    public Rompecabezas(int filas, int columnas, int numPiezas) {
        /*
        Para crear el rompecabezas se ha basaso en evaluar casos:
        
        CASO 1: la pieza no tiene otra pieza al lado izquierdo ni arriba, se crea una pieza completamente random
        
        CASO 2: La pieza no tiene otra pieza al arriba, pero si al lado izquierdo,
            el lado izquiedo de la nueva pieza se saca de la pieza al lado izquierdo, posición derecha
        
        CASO 3: La pieza no tiene otra pieza lado izquierdo, pero si arriba,
            el lado arriba de la nueva pieza se saca de la pieza de arriba, posición abajo
        
        CASO 4: La pieza tiene piza tanto al lado izquierdo como arriba,
            el lado izquiedo de la nueva pieza se saca de la pieza al lado izquierdo, posición derecha y 
            l lado arriba de la nueva pieza se saca de la pieza de arriba, posición abajo
        */
        this.rompecabezas=new Pieza[filas][columnas];
        this.desordenado= new ArrayList<>();
        this.numComlunas=columnas;
        this.finas=filas;
        this.columnas=columnas;
        
        for(int i=0; i<filas; i++){
            for(int j=0; j<columnas; j++){
               if(i-1<0 && j-1<0){//CASO 1
                   rompecabezas[i][j]=new Pieza((int)(random()*(numPiezas+1)), (int)(random()*(numPiezas+1)),
                           (int)(random()*(numPiezas+1)), (int)(random()*(numPiezas+1)));
               }else if(i-1<0 && j-1>=0){//CASO 2
                   
                   Pieza piezaCalza=rompecabezas[i][j-1];
                   rompecabezas[i][j]=new Pieza(piezaCalza.getDerecha(), (int)(random()*(numPiezas+1)),
                           (int)(random()*(numPiezas+1)), (int)(random()*(numPiezas+1)));
               }else if(i-1>=0 && j-1<0){//CASO 3
                   
                   Pieza piezaCalza=rompecabezas[i-1][j];
                   rompecabezas[i][j]=new Pieza((int)(random()*(numPiezas+1)), (int)(random()*(numPiezas+1)),
                           piezaCalza.getAbajo(), (int)(random()*(numPiezas+1)));
               }else{//CASO 4
                   Pieza piezaCalza1=rompecabezas[i][j-1];
                   Pieza piezaCalza2=rompecabezas[i-1][j];
                   rompecabezas[i][j]=new Pieza(piezaCalza1.getDerecha(), (int)(random()*(numPiezas+1)),
                           piezaCalza2.getAbajo(), (int)(random()*(numPiezas+1)));
               }
        
            }
        }
        numPieza=0;
        this.desordenarRompecabezas();//Una vez creado el rompecabezas se va a desordenar
    }
    
    void desordenarRompecabezas(){
        List<Pieza> temp = new ArrayList<>();//Se almcenara en una sola lista todas las piezas del rompecabezas
        ordenado = new ArrayList<>();
        for (Pieza[] rompecabeza : rompecabezas) { //Se pasan las piezas de la matriz a la lista
            temp.addAll(Arrays.asList(rompecabeza));
        }
        for(Pieza p: temp){
            ordenado.add(new Pieza(p.getIzquierda(), p.getDerecha(), p.getArriba(), p.getAbajo()));
        }
        Collections.shuffle(temp);//Se desordena la lista.
        int cont=0;
        piezas=new Pieza[rompecabezas.length*rompecabezas[0].length];
        board=new int[rompecabezas[0].length][rompecabezas.length];
        boardBruto=new int[rompecabezas[0].length][rompecabezas.length];
        for(int i=0; i<rompecabezas.length; i++){//Se mueven las piezas de la lista a la matriz
            for(int j=0; j<rompecabezas[i].length; j++){
                board[i][j]=-1;
                boardBruto[i][j]=cont;
                piezas[cont]=temp.get(cont);
                rompecabezas[i][j]=temp.get(cont);
                cont++;
            }
        }
        desordenado=temp; 
    }  
    
    //******************************METODO BRUTO********************************************++
    static int piezasColocadas=0;
    static boolean banderaSalida=false;
    public void ordenarMetodoBruto(List<Pieza> temp){     
        
        if(temp.size()==desordenado.size()){
            imprimeRompezabezas(temp);
            banderaSalida=true;
            return;    
        }
        
        for(int i=0; i<desordenado.size(); i++){
            if(banderaSalida)return;
            
            if(!(temp).contains(desordenado.get(i))){
      
                Pieza p= desordenado.get(i);
                if(temp.isEmpty()){
                    temp.add(p);
                    piezasColocadas++;
                    ordenarMetodoBruto(temp); 
                    temp.remove(p);
                    piezasColocadas--;
                    
                }else if(piezasColocadas%columnas==0){
                    if(temp.get(temp.size()-numComlunas).getAbajo()==p.getArriba()){
                        temp.add(p);
                        piezasColocadas++;
                        ordenarMetodoBruto(temp); 
                        temp.remove(p);
                        piezasColocadas--;
                    }
                    
                }else if(temp.size()>numComlunas){
                    if(temp.get(temp.size()-1).getDerecha()==p.getIzquierda() &&
                        temp.get(temp.size()-numComlunas).getAbajo()==p.getArriba()){
                    
                        temp.add(p);
                        piezasColocadas++;
                        ordenarMetodoBruto(temp); 
                        temp.remove(p);
                        piezasColocadas--;
                    }
                }else if(temp.size()<numComlunas){
                    if(temp.get(temp.size()-1).getDerecha()==p.getIzquierda()){

                        temp.add(p);
                        piezasColocadas++;
                        ordenarMetodoBruto(temp); 
                        temp.remove(p);
                        piezasColocadas--;
                }
                }
            }
        }
        
    }
    public void imprimeRompezabezas(List<Pieza> p){
        piezasColocadas=0;
        int a=numComlunas;
        Pieza pz[]=new Pieza[p.size()];
        for(int j=0; j<p.size(); j++){
            pz[j]=p.get(j);
        }
       
        System.out.println(new TetravexBoard(finas, columnas, pz, boardBruto).toString());
            
    }

    public String getComplejidad(){
        banderaSalida=false;
        piezasColocadas=0;
        long asignaciones1=asignaciones;
        long comparaciones1=comparaciones;
        asignaciones=0;
        comparaciones=0;
        return "   Asignaciones --->"+asignaciones1+"    Comparaciones --->"+comparaciones1;
    }
    
    public void avanceRapido()
    {
        TetravexBoard tetravex = new TetravexBoard(columnas, finas, piezas, board);

        Solver<TetravexMove> solver = new Solver<TetravexMove>();
        solver.setInitialState(tetravex);
        solver.setRevisitStates(true);
        
        try{
        solver.solve();
        }catch(OutOfMemoryError e){
            System.out.println("NO SE PUDO");
        }

        //System.out.format("Solved in %d ms\n", solver.getSolveTime());
        //System.out.format("States created: %d\n", solver.getStatesCreated());
        //System.out.format("States visited: %d\n", solver.getStatesVisited());
        //System.out.format("States duplicated: %d\n", solver.getStatesDuplicates());
        //System.out.format("States pooled: %d\n", solver.getUniqueStates());
        //System.out.format("States branched: %d\n", solver.getStatesDeviated());

        List<TetravexBoard> solutions = solver.getSolutions();

        //System.out.format("Solutions (%d) {\n", solutions.size());
        for (TetravexBoard b : solutions) {
                System.out.println(b);
        }
        
        System.out.println();
}
   
}

