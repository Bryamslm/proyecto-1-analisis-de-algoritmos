/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TetravexSolver;

/**
 *
 * @author bryam
 */
public class Pieza {
    public static final int EMPTY = -1;
	public static final int LEFT = 0;
	public static final int TOP = 1;
	public static final int RIGHT = 2;
	public static final int BOTTOM = 3;

	public int[] s = new int[4];
	public int index;

	public Pieza( int left, int right, int top, int bottom )
	{
		s[LEFT] = left;
		s[TOP] = top;
		s[RIGHT] = right;
		s[BOTTOM] = bottom;
	}
    public int getIzquierda(){
        return this.s[LEFT];
    }
    public int getDerecha(){
        return this.s[RIGHT];
    }
    public int getArriba(){
        return this.s[TOP];
    }
    public int getAbajo(){
        return this.s[BOTTOM];
    }

	public int get( int index )
	{
		return s[index & 3];
	}

	public boolean match( Pieza ideal )
	{
		if (ideal.s[LEFT] != EMPTY && ideal.s[LEFT] != s[LEFT])
		{
			return false;
		}
		if (ideal.s[TOP] != EMPTY && ideal.s[TOP] != s[TOP])
		{
			return false;
		}
		if (ideal.s[TOP] != EMPTY && ideal.s[TOP] != s[TOP])
		{
			return false;
		}
		if (ideal.s[BOTTOM] != EMPTY && ideal.s[BOTTOM] != s[BOTTOM])
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder( 40 );
		sb.append( "+-----+\n" );
		sb.append( "|\\ " ).append( s[TOP] ).append( " /|\n" );
		sb.append( "|" ).append( s[LEFT] ).append( " X " ).append( s[RIGHT] ).append( "|\n" );
		sb.append( "|/ " ).append( s[BOTTOM] ).append( " \\|\n" );
		sb.append( "+-----+\n" );
		return sb.toString();
	}

}
