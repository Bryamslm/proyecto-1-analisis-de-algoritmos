/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TetravexSolver;

import java.util.ArrayList;

/**
 *
 * @author bryam
 */
public class Main {
    
    public static void main(String[] args) 
	{

        Rompecabezas rompecabezas;//=new Rompecabezas(3, 3, 9);
        //rompecabezas.ordenarMetodoBruto(new ArrayList<>());
            
        int tamagnos[]={3, 4, 5, 6, 7, 8};
        for(int i: tamagnos){
            System.out.println("------------------------------------------------------------------------------------\n");
            System.out.println("MÉTODO BRUTO (Espere).....");
            rompecabezas=new Rompecabezas(i, i, 9);
            rompecabezas.ordenarMetodoBruto(new ArrayList<>());
            System.out.print("Listo --> "+i+"X"+i+rompecabezas.getComplejidad()+"\n");
            
        }
		
	}
}
