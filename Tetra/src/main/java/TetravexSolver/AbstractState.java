/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TetravexSolver;

import java.util.Iterator;

/**
 *
 * @author bryam
 * @param <M>
 */
public class AbstractState<M> implements State<M> {
    // The depth of the state
	private int depth;

	// The parent state
	private State<M> parent;

	@Override
	public int getDepth()
	{
		return depth;
	}

	@Override
	public State<M> getParent()
	{
		return parent;
	}

	@Override
	public void setDepth( int depth )
	{
		this.depth = depth;
	}

	@Override
	public void setParent( State<M> parent )
	{
		this.parent = parent;
	}
	
	@Override
	public int getRemainingMoves()
	{
		return -1;
	}

    @Override
    public boolean isSolution() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterator<M> getMoves() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public State<M> getCopy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addMove(M move) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getHash() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
